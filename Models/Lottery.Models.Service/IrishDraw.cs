﻿using System;

namespace Lottery.Models.Service
{
    public class IrishDraw : IEquatable<IrishDraw>
    {
        public DateTime DrawDate { get; set; }
        public IrishLine Main { get; set; }
        public IrishLine PlusOne { get; set; }
        public IrishLine PlusTwo { get; set; }
    
        public override bool Equals(object obj)
        {
            return this.Equals(obj as IrishDraw);
        }

        public bool Equals(IrishDraw other)
        {
            if (other == null)
                return false;

            return this.DrawDate.Equals(other.DrawDate) &&
                (
                    object.ReferenceEquals(this.Main, other.Main) ||
                    this.Main != null &&
                    this.Main.Equals(other.Main)
                ) &&
                (
                    object.ReferenceEquals(this.PlusOne, other.PlusOne) ||
                    this.PlusOne != null &&
                    this.PlusOne.Equals(other.PlusOne)
                ) &&
                (
                    object.ReferenceEquals(this.PlusTwo, other.PlusTwo) ||
                    this.PlusTwo != null &&
                    this.PlusTwo.Equals(other.PlusTwo)
                );
        }

    }
}
