﻿using System;

namespace Lottery.Models.Service
{
    public class IrishLine : IEquatable<IrishLine>
    {
        public int BallOne { get; set; }
        public int BallTwo { get; set; }
        public int BallThree { get; set; }
        public int BallFour { get; set; }
        public int BallFive { get; set; }
        public int BallSix { get; set; }
        public int BonusBall { get; set; }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as IrishLine);
        }

        public bool Equals(IrishLine other)
        {
            if (other == null)
                return false;

            return this.BallOne.Equals(other.BallOne) &&
                this.BallTwo.Equals(other.BallTwo) &&
                this.BallThree.Equals(other.BallThree) &&
                this.BallFour.Equals(other.BallFour) &&
                this.BallFive.Equals(other.BallFive) &&
                this.BallSix.Equals(other.BallSix) &&
                this.BonusBall.Equals(other.BonusBall);
        }
    }
}
