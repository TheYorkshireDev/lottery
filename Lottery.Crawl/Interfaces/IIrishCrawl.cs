﻿using System;
using System.Threading.Tasks;
using Lottery.Models.Service;

namespace Lottery.Crawl.Interfaces
{
    public interface IIrishCrawl
    {
        Task<IrishDraw> GetLotteryIE(string url);
        Task<IrishDraw> GetLottoLand(string url);
        Task<IrishDraw> GetIrishNational(string url);
    }
}
