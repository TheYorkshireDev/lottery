﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using HtmlAgilityPack;
using Lottery.Crawl.Interfaces;
using Lottery.Models.Service;

namespace Lottery.Crawl
{
    public class IrishCrawl : IIrishCrawl
    {
        private static HttpClient client = new HttpClient();

        public async Task<IrishDraw> GetLotteryIE(string url)
        {
            var htmlString = GetWebpage(url);
            return ParseLotteryIE(await htmlString);
        }

        public async Task<IrishDraw> GetLottoLand(string url)
        {
            var htmlString = GetWebpage(url);
            return ParseLottoLand(await htmlString);
        }

        public async Task<IrishDraw> GetIrishNational(string url)
        {
            var htmlString = GetWebpage(url);
            return ParseIrishNational(await htmlString);
        }

        public async Task<string> GetWebpage(string url)
        {
            return await client.GetStringAsync(url);
        }

        public IrishDraw ParseLotteryIE(string webpageHtml)
        {
            try
            {
                var htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(webpageHtml);

                var divs = htmlDocument.DocumentNode.Descendants("div")
                    .Where(node => node.GetAttributeValue("class", "").Equals("winning-results")).ToList();

                var drawSection = htmlDocument.DocumentNode.Descendants("section")
                    .Where(node => node.GetAttributeValue("class", "").Equals("matching-draw")).First();

                var date = drawSection.Descendants("h4").FirstOrDefault().InnerText;
                var dt = Convert.ToDateTime(date);

                // Get Main Draw
                var mainDraw = divs.Where(node => node.Descendants("div").First().GetAttributeValue("class", "").Equals("results-logo results-logo-lotto")).Single();
                var plusOneDraw = divs.Where(node => node.Descendants("div").First().GetAttributeValue("class", "").Equals("results-logo results-logo-lotto-plus-one")).Single();
                var plusTwoDraw = divs.Where(node => node.Descendants("div").First().GetAttributeValue("class", "").Equals("results-logo results-logo-lotto-plus-two")).Single();

                return new IrishDraw
                {
                    DrawDate = dt,
                    Main = GetIENumbers(mainDraw),
                    PlusOne = GetIENumbers(plusOneDraw),
                    PlusTwo = GetIENumbers(plusTwoDraw)
                };
            } catch (Exception)
            {
                return null;
            }
        }

        public IrishDraw ParseLottoLand(string webpageHtml)
        {
            try
            {
                var htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(webpageHtml);

                var ballLists = htmlDocument.DocumentNode.Descendants("ul")
                    .Where(node => node.GetAttributeValue("class", "").Contains("l-lottery-numbers-container")).ToList();

                var drawSection = htmlDocument.DocumentNode.Descendants("header")
                    .Where(node => node.GetAttributeValue("class", "").Equals("l-heading_6 l-h-padding-top-bottom")).First();

                //TODO: Fix Date Issue
                var dateText = drawSection.InnerText;
                var toBeSearched = "Results for ";
                var date = dateText.Substring(dateText.IndexOf(toBeSearched) + toBeSearched.Length);
                var dt = Convert.ToDateTime(date);

                // Get Main Draw
                var mainDraw = ballLists.Single(node => node.GetAttributeValue("class", "").Equals("l-lottery-numbers-container irishLotto t-results-balls"));
                var plusOneDraw = ballLists.Single(node => node.GetAttributeValue("class", "").Equals("l-lottery-numbers-container l-results-numbers lottoPlus1"));
                var plusTwoDraw = ballLists.Single(node => node.GetAttributeValue("class", "").Equals("l-lottery-numbers-container l-results-numbers lottoPlus2"));

                return new IrishDraw
                {
                    DrawDate = dt,
                    Main = GetLottoLandNumbers(mainDraw),
                    PlusOne = GetLottoLandNumbers(plusOneDraw),
                    PlusTwo = GetLottoLandNumbers(plusTwoDraw)
                };
            } catch (Exception)
            {
                return null;
            }
        }

        public IrishDraw ParseIrishNational(string webpageHtml)
        {
            try
            {
                var htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(webpageHtml);

                var resultDiv = htmlDocument.DocumentNode.Descendants("div")
                    .Single(node => node.GetAttributeValue("class", "").Equals("resultsOuter full fluid"));

                var dateDiv = resultDiv.Descendants("div")
                    .Single(node => node.GetAttributeValue("class", "").Equals("resultsHeader irish-lotto"));

                //TODO: Fix Date Issue
                var dateText = dateDiv.InnerText;
                var removedNewlines = dateText.Replace("\r\n","").Trim();
                var removedTabs = removedNewlines.Replace("\t","").Trim();
                var removedDay = removedTabs.Remove(0, removedTabs.IndexOf(' ') + 1);
                var removeDaySuffix = removedDay.Substring(0,4)
                            .Replace("nd","")
                            .Replace("th","")
                            .Replace("rd","")
                            .Replace("st","")
                            + removedDay.Substring(4);

                var dt = Convert.ToDateTime(removeDaySuffix);

                //Saturday 4th November 2017

                var ballLists = resultDiv.Descendants("div")
                    .Where(node => node.GetAttributeValue("class", "").Equals("balls")).ToList();

                // Get Main Draw
                var mainDraw = ballLists[0];
                var plusOneDraw = ballLists[1];
                var plusTwoDraw = ballLists[2];

                return new IrishDraw
                {
                    DrawDate = dt,
                    Main = GetIrishNationalNumbers(mainDraw),
                    PlusOne = GetIrishNationalNumbers(plusOneDraw),
                    PlusTwo = GetIrishNationalNumbers(plusTwoDraw)
                };
            } catch (Exception)
            {
                return null;
            }
        }

        private static IrishLine GetIENumbers(HtmlNode div)
        {
            var mainNumbers = div.Descendants("div").Where(node => node.GetAttributeValue("class", "").Equals("pick-number")).ToList();
            var bonus = div.Descendants("div").Where(node => node.GetAttributeValue("class", "").Equals("bonus")).First();
            var line = new IrishLine
            {
                BallOne = Int32.Parse(mainNumbers[0].Descendants("label").FirstOrDefault().InnerText),
                BallTwo = Int32.Parse(mainNumbers[1].Descendants("label").FirstOrDefault().InnerText),
                BallThree = Int32.Parse(mainNumbers[2].Descendants("label").FirstOrDefault().InnerText),
                BallFour = Int32.Parse(mainNumbers[3].Descendants("label").FirstOrDefault().InnerText),
                BallFive = Int32.Parse(mainNumbers[4].Descendants("label").FirstOrDefault().InnerText),
                BallSix = Int32.Parse(mainNumbers[5].Descendants("label").FirstOrDefault().InnerText),
                BonusBall = Int32.Parse(bonus.Descendants("label").FirstOrDefault().InnerText)
            };
            return line;
        }

        private static IrishLine GetLottoLandNumbers(HtmlNode list)
        {
            var mainNumbers = list.Descendants("li").Where(node => node.GetAttributeValue("class", "").Equals("l-lottery-number  ")).ToList();
            var bonus = list.Descendants("li").Single(node => node.GetAttributeValue("class", "").Contains("l-lottery-number extra "));
            var line = new IrishLine
            {
                BallOne = Int32.Parse(mainNumbers[0].InnerText),
                BallTwo = Int32.Parse(mainNumbers[1].InnerText),
                BallThree = Int32.Parse(mainNumbers[2].InnerText),
                BallFour = Int32.Parse(mainNumbers[3].InnerText),
                BallFive = Int32.Parse(mainNumbers[4].InnerText),
                BallSix = Int32.Parse(mainNumbers[5].InnerText),
                BonusBall = Int32.Parse(bonus.InnerText)
            };
            return line;
        }

        private static IrishLine GetIrishNationalNumbers(HtmlNode list)
        {
            var mainNumbers = list.Descendants("div").Where(node => node.GetAttributeValue("class", "").Equals("result irish-lotto-ball fluid")).ToList();
            var bonus = list.Descendants("div").Single(node => node.GetAttributeValue("class", "").Contains("result irish-lotto-bonus-ball fluid"));
            var line = new IrishLine
            {
                BallOne = Int32.Parse(mainNumbers[0].InnerText),
                BallTwo = Int32.Parse(mainNumbers[1].InnerText),
                BallThree = Int32.Parse(mainNumbers[2].InnerText),
                BallFour = Int32.Parse(mainNumbers[3].InnerText),
                BallFive = Int32.Parse(mainNumbers[4].InnerText),
                BallSix = Int32.Parse(mainNumbers[5].InnerText),
                BonusBall = Int32.Parse(bonus.InnerText)
            };
            return line;
        }
    }
}