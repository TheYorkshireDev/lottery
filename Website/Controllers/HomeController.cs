﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Website.Models;
using AutoMapper;
using Lottery.Service.Interface;
using Lottery.Models.Service;

namespace Website.Controllers
{
    public class HomeController : Controller
    {
        private readonly IIrishService _irishService;
        private readonly IMapper _mapper;

        public HomeController(IIrishService irishService, IMapper mapper)
        {
            _irishService = irishService;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            (IrishDraw irishDraw, string errMsg) = await _irishService.GetIrishDraw();
            ViewData["ErrorMessage"] = errMsg;
            
            if (irishDraw == null)
            {
                return View(new ResultsViewModel());
            }

            var irishResult = _mapper.Map<IrishViewModel>(irishDraw);

            // var nationalResult = new NationalViewModel
            // {
            //     DrawDate = irishDraw.DrawDate,
            //     Main = new NationalLineViewModel
            //     {
            //         BallOne = irishDraw.Main.BallOne.ToString("00"),
            //         BallTwo = irishDraw.Main.BallTwo.ToString("00"),
            //         BallThree = irishDraw.Main.BallThree.ToString("00"),
            //         BallFour = irishDraw.Main.BallFour.ToString("00"),
            //         BallFive = irishDraw.Main.BallFive.ToString("00"),
            //         BallSix = irishDraw.Main.BallSix.ToString("00"),
            //         BonusBall = irishDraw.Main.BonusBall.ToString("00")
            //     }
            // };

            // var euroResult = new EuroViewModel
            // {
            //     DrawDate = irishDraw.DrawDate,
            //     Main = new EuroLineViewModel
            //     {
            //         BallOne = irishDraw.Main.BallOne.ToString("00"),
            //         BallTwo = irishDraw.Main.BallTwo.ToString("00"),
            //         BallThree = irishDraw.Main.BallThree.ToString("00"),
            //         BallFour = irishDraw.Main.BallFour.ToString("00"),
            //         BallFive = irishDraw.Main.BallFive.ToString("00"),
            //         LuckyStarOne = irishDraw.Main.BallSix.ToString("00"),
            //         LuckyStarTwo = irishDraw.Main.BonusBall.ToString("00")
            //     }
            // };

            var resultsViewModel = new ResultsViewModel
            {
                Irish = irishResult
                // National = nationalResult,
                // Euro = euroResult
            };

            return View(resultsViewModel);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
