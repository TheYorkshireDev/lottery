using AutoMapper;
using Lottery.Models.Service;
using Website.Models;

namespace Website
{
    public class ViewModelProfile : Profile
    {
        public ViewModelProfile()
        {
            // Models to ViewModels
            CreateMap<IrishLine, IrishLineViewModel>();
            CreateMap<IrishDraw, IrishViewModel>();
            CreateMap<int, string>().ConvertUsing<StringTypeConverter>();
        }
    }

    public class StringTypeConverter : ITypeConverter<int, string>
    {
        public string Convert(int source, string destination, ResolutionContext context)
        {
            return source.ToString("00");
        }
    }
}