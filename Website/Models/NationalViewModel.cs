using System;

namespace Website.Models
{
    public class NationalViewModel
    {
        public DateTime DrawDate { get; set; }
        public NationalLineViewModel Main { get; set; }
    }
}