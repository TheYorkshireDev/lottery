using System;

namespace Website.Models
{
    public class EuroLineViewModel
    {
        public string BallOne { get; set; }
        public string BallTwo { get; set; }
        public string BallThree { get; set; }
        public string BallFour { get; set; }
        public string BallFive { get; set; }
        public string LuckyStarOne { get; set; }
        public string LuckyStarTwo { get; set; }
    }
}