using System;

namespace Website.Models
{
    public class EuroViewModel
    {
        public DateTime DrawDate { get; set; }
        public EuroLineViewModel Main { get; set; }
    }
}