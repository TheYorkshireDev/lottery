using System;

namespace Website.Models
{
    public class IrishViewModel
    {
        public DateTime DrawDate { get; set; }
        public IrishLineViewModel Main { get; set; }
        public IrishLineViewModel PlusOne { get; set; }
        public IrishLineViewModel PlusTwo { get; set; }
    }
}