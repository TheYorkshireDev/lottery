using System;

namespace Website.Models
{
    public class NationalLineViewModel
    {
        public string BallOne { get; set; }
        public string BallTwo { get; set; }
        public string BallThree { get; set; }
        public string BallFour { get; set; }
        public string BallFive { get; set; }
        public string BallSix { get; set; }
        public string BonusBall { get; set; }
    }
}