using System;

namespace Website.Models
{
    public class ResultsViewModel
    {
        public IrishViewModel Irish { get; set; }
        public NationalViewModel National { get; set; }
        public EuroViewModel Euro { get; set; }
    }
}