FROM microsoft/aspnetcore-build:2.0 AS build-env
WORKDIR /solution

# Copy all solution files and restore as distinct layers
COPY . ./
RUN dotnet restore

# Move into the specifiec app and publish
RUN cd Website/ && dotnet publish -c Release -o out

# Build runtime image
FROM microsoft/aspnetcore:2.0
WORKDIR /app
COPY --from=build-env /solution/Website/out .
ENTRYPOINT ["dotnet", "Website.dll"]