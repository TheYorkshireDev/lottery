﻿using System;
using System.Threading.Tasks;
using Lottery.Service.Interface;
using Lottery.Crawl.Interfaces;
using Lottery.Models.Service;

namespace Lottery.Service.Irish
{
    public class IrishService : IIrishService
    {
        private readonly IIrishCrawl _crawl;
        private static string LotteryIEUrl = @"https://www.lottery.ie/dbg/results/view";
        private static string LottoLandUrl = @"https://www.lottoland.co.uk/irish-lottery/results-winning-numbers";
        private static string IrishNationalUrl = @"https://irish.national-lottery.com/irish-lotto/results";

        private static string ErrorMessage = "{0} has different results for this draw";

        public IrishService(IIrishCrawl crawl)
        {
            _crawl = crawl;
        }

        public async Task<(IrishDraw, string)> GetIrishDraw()
        {
            // Check if we have cached it.
            // See if we can look up the date. Is it wednesday or saturday after 9pm for example?

            // Potentially return the cached value

            // Call and get website 1 draw
            var LotteryIEResult = await _crawl.GetLotteryIE(LotteryIEUrl);
            // Call and get website 2 draw
            var LottoLandResult = await _crawl.GetLottoLand(LottoLandUrl);
            // Call and get website 3 draw
            var IrishNationalResult = await _crawl.GetIrishNational(IrishNationalUrl);

            // Return the results which have the most similarity
            if (LotteryIEResult != null && LotteryIEResult.Equals(LottoLandResult) && LotteryIEResult.Equals(IrishNationalResult))
            {
                return (LotteryIEResult, "");
            }

            if (LotteryIEResult != null && LotteryIEResult.Equals(LottoLandResult))
            {
                // IrishNationalResult did not match
                return (LotteryIEResult, string.Format(ErrorMessage, "irish.national-lottery.com"));
            }

            if (LotteryIEResult != null && LotteryIEResult.Equals(IrishNationalResult))
            {
                // LottoLandResult did not match
                return (LotteryIEResult, string.Format(ErrorMessage, "www.lottoland.co.uk"));
            }

            if (LottoLandResult != null && LottoLandResult.Equals(IrishNationalResult))
            {
                // LotteryIEResult did not match
                return (LottoLandResult, string.Format(ErrorMessage, "www.lottery.ie"));
            }

            return (null, "Could not find any results");
        }
    }
}
