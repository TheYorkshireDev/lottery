﻿using System;
using System.Threading.Tasks;
using Lottery.Models.Service;

namespace Lottery.Service.Interface
{
    public interface IIrishService
    {
        Task<(IrishDraw, string)> GetIrishDraw();
    }
}
